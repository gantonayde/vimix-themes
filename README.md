<div align="center">
<h1 align="center">
  <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/18550775/vimix_logo.jpg" alt="Project">
  <br />
  Vimix Themes for Snap
</h1>
</div>

<p align="center"><b>These are the Vimix GTK and icon themes for snap apps.</b> Vimix is a flat Material Design theme for GTK 3, GTK 2 and Gnome-Shell which supports GTK 3 and GTK 2 based desktop environments like Gnome, Unity, Budgie, Pantheon, XFCE, Mate, etc.

This theme is based on the <a href="https://github.com/nana-4/materia-theme">Materia theme</a> of nana-4. 

This snap contains all flavours for both the GTK and icon themes.

<b>Attributions</b>:
This snap is packaged from vinceliuice's <a href="https://github.com/vinceliuice/vimix-gtk-themes">Vimix-gtk-themes</a> and <a href="https://github.com/vinceliuice/vimix-icon-theme">Vimix-icon-theme</a>. The Vimix-gtk-themes and Vimix-icon-theme are under a GPL3.0 licence.

</p>


## Install <a href="https://snapcraft.io/vimix-themes">
<a href="https://snapcraft.io/vimix-themes">
<img alt="Get it from the Snap Store" src="https://snapcraft.io/static/images/badges/en/snap-store-white.svg" />
</a>

You can install the snap from the Snap Store оr by running:
```
sudo snap install vimix-themes
```
To connect the theme to an app run:
```
sudo snap connect [other snap]:gtk-3-themes vimix-themes:gtk-3-themes 
```
```
sudo snap connect [other snap]:icon-themes vimix-themes:icon-themes
```

